resource "aws_s3_bucket" "new" {
  bucket = "sdk-sync"
  acl    = "private"
}

resource "aws_ami_from_instance" "example" {
  name               = "untar-test-box"
  source_instance_id = "i-0a52cf85a10565545"
}

resource "aws_ami_launch_permission" "share" {
  image_id   = "ami-0229f8e735cf38eef"
  account_id = "740975335541"
}

resource "aws_iam_role" "s3_share_role" {
  name = "s3_share_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "AWS": "arn:aws:iam::AccountB:user/AccountBUserName"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF