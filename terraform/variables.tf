variable "profile" {
  description = "AWS Profile"
  default     = "personal"
}
variable "region" {
  description = "AWS region"
  default     = "us-east-1"
}

variable "ami_id" {
  description = "ami-0229f8e735cf38eef"
}

variable "account_id" {
  description = "973590762683"
  type = string
}
